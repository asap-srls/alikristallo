<?
$pag="home";
include('include/config.php');
include 'functions/session.php';
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include 'include/head.php' ?>
    
</head>
  <body >
    <? include 'include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include 'include/left_nav.php'; ?>
        <!--/col-->

        <div class="col-md-9 col-lg-10 main">

            <!--toggle sidebar button-->
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>

            <h1 class="display-1 hidden-xs-down">
            <!-- <svg class="svg-icon">
                <use xlink:href="#team" />
            </svg> --> Fast Dashboard
            </h1>
            <p class="lead hidden-xs-down">(Le aree più utilizzate)</p>
            

            <div class="row mb-3">
                <div class="col-lg-3 col-md-6">
                    <a href="<? echo $root ?>lingue">
                        <div class="card home-card card-inverse card-success">
                            <div class="card-block bg-success">
                                <div class="rotate">
                                    <i class="fa fa-heart fa-5x"></i>
                                </div>
                                <h6 class="text-uppercase">Lingue</h6>
                                <h1 class="display-1">2</h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="<? echo $root ?>prodotti">
                        <div class="card home-card card-inverse card-danger">
                            <div class="card-block bg-danger">
                                <div class="rotate">
                                    <i class="fa fa-list fa-5x"></i>
                                </div>
                                <h6 class="text-uppercase">Prodotti</h6>
                                <h1 class="display-1">
                                <?
                                    $q2 = $db->prepare("SELECT * FROM prodotti_it");
                                    $q2->execute();
                                    echo $q2->rowCount();
                                ?></h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="<? echo $root ?>immagini">
                        <div class="card home-card card-inverse card-info">
                            <div class="card-block bg-info">
                                <div class="rotate">
                                    <i class="fa fa-hand-peace-o fa-5x"></i>
                                </div>
                                <h6 class="text-uppercase">immagini</h6>
                                <h1 class="display-1">∞</h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <a href="<? echo $root ?>tutorial">
                        <div class="card home-card card-inverse card-warning">
                            <div class="card-block bg-warning">
                                <div class="rotate">
                                    <i class="fa fa-hand-spock-o fa-5x"></i>
                                </div>
                                <h6 class="text-uppercase">tutorial</h6>
                                <h1 class="display-1">4</h1>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<? include 'include/footer.php'; ?>
  </body>
</html>