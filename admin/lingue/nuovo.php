<?
$pag=basename(__DIR__);
if (isset($_GET['id']) || $_GET['id']!='') {  
  $id = $_GET['id'];
}else{
  $id="";
}
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';
include '../moduli/moduli.php';

//PREPARAZIONE ALLA PAGINA
// Se è settato l'id allora imposto la modifica in true, il che vuol dire che mi farà
// il collegamento a database per prendere i dati, se invece non è settato prendo l'ultimo id inserito, lo aumento di 1
// e lo setto come id

$hasFile = false; //setto se la pagina ha oppure no dei file, verrà richiamata la funzione giusta nel footer

?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <div class="col-md-9 col-lg-10 main">
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>
            
              <h1 class="display-1 hidden-xs-down">
               <? 
                echo $pag ." - " . strtoupper($id);
                
               ?>
              </h1>
              <div class="alert alert-warning" role="alert">
                <strong>ATTENZIONE!</strong><br/>
                I valori possono contenere codici HTML al loro interno. Stare attenti durante la modifica.<br/><br/>
                <strong>SE SI VUOLE CAMBIARE UN'IMMAGINE</strong><br/>
                prima assicurati che sia presente tra le immagini caricate, andando nella sezione <a href="<?= $root ?>immagini/index.php">immagini</a>. Nel caso non ci fosse, caricarla li, e copiare il nome, cliccando sul bottone rosso con scritto "COPIA LINK" posto sopraogni immagine o video.<br/>
                <strong>es: se la cartella dove si fa l'upload si chiamerà "upload", e l'immagine si chiamerà "prova.jpg", il valore da inserire nella chiave di lingua sarà "upload/prova.jpg"</strong>
              </div>
              <div class="col-md-8 offset-md-2 text-center"><label for="myInput">Fai una ricerca in base al codice della stringa o al suo valore</label></div>
              <div class="input-group col-md-8 offset-md-2">
                <span class="input-group-addon"><i class="fa fa-search fa-fw"></i></span><input id="myInput" type="text" class="col-12 form-control" placeholder="Ricerca codice stringa o valore">    
              </div>
              <form method="post" id="lngForm">
                <div id="explode_lng">
            <? 
              echo '<input type="hidden" id="lng" name="lng" value="'.$id.'">';
              echo "<div class='row align-items-center mt-3 mb-3'>";
              echo "<div class='col-md-3 text-md-right'><h5>CODICE STRINGA</h5></div><div class='col-md-4'><h5>VALORE IT</h5></div><div class='col-md-5'><h5>VALORE</h5></div><hr>";
              echo "</div>";

              $file_path = "../../dictionaries/{$id}.properties";
                
                   
            $lines = explode("\n", trim(file_get_contents($file_path)));
            

            foreach ($lines as $line) {
                $line = trim($line);

                if (!$line || substr($line, 0, 1) == '#') // Saltiamo le linee vuote e i commenti
                continue;

                if (false !== ($pos = strpos($line, '='))) {
                    // $convert[trim(substr($line, 0, $pos))] = trim(substr($line, $pos + 1));
                    $cod = trim(substr($line, 0, $pos));
                    echo "<div class='row align-items-center single'>";
                    echo "<div class='col-md-3 text-md-right'><h5>".trim(substr($line, 0, $pos)) . "</h5></div>
                          <div class='col-md-4'><p>".htmlspecialchars($convert[$cod])."</p></div>
                          <div class='input-group col-md-5'><textarea style='width:100%' name='".trim(substr($line, 0, $pos))."' rows='4'>". htmlspecialchars(trim(substr($line, $pos + 1)))."</textarea></div><hr>";
                    echo "</div>";
                }
            }
            ?>
            
                 </div>                  
              

                          
              
              
              <div class="col-lg-12 text-right save-btn">
                <a id="submit_lng" class="btn red-bg">SALVA <i class="fa fa-chevron-right fa-lg"></i></a>
              </div>
            </form> 
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserimento <? echo $pag ?></h4>
            </div>
            <div class="modal-body" id="contenutoModal">
                
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary-outline" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<? 
include '../include/footer.php'; 
// chiudo la connessione a MySQL
?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#explode_lng .single").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
    $('#submit_lng').on('click', function(e){
      e.preventDefault();
      var formLng = $("#lngForm")[0];
      var strLng = new FormData(formLng);
      $.ajax({    
            type: "POST",  
            url: "change_lng.php",
            data: strLng,
            contentType: false,
            processData: false,
            success: function(msg)
            {
              $("#contenutoModal").html(msg);
              $("#successModal").modal('show');
            },
            error: function()
            {
              alert("errore nella prima chiamata"); //sempre meglio impostare una callback in caso di fallimento
            }
      });
    })
  });
</script>

  </body>
</html>