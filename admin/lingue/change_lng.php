<?
include_once '../include/config.php';
include '../include/languages.php';
// include 'functions.php';



function replace_in_file($FilePath, $OldText, $NewText)
{
    $Result = array('status' => 'error', 'message' => '');
    if(file_exists($FilePath)===TRUE)
    {
        if(is_writeable($FilePath))
        {
            try
            {
                $FileContent = file_get_contents($FilePath);
                $FileContent = str_replace($OldText, $NewText, $FileContent);
                if(file_put_contents($FilePath, $FileContent) > 0)
                {
                    $Result["status"] = 'success';
                    $Result["message"] = $FilePath." ". $OldText." ". $NewText;
                }
                else
                {
                   $Result["message"] = 'Error while writing file';
                }
            }
            catch(Exception $e)
            {
                $Result["message"] = 'Error : '.$e;
            }
        }
        else
        {
            $Result["message"] = 'File '.$FilePath.' is not writable !';
        }
    }
    else
    {
        $Result["message"] = 'File '.$FilePath.' does not exist !';
    }
    return $Result;
    echo $Result['status'] . " - " . $Result['message'];
}



$lng = $_POST['lng'];
$file_path = "../../dictionaries/".$lng.".properties";
$lines = explode("\n", trim(file_get_contents($file_path)));
foreach ($lines as $line) {
  $line = trim($line);
  if (!$line || substr($line, 0, 1) == '#') // Saltiamo le linee vuote e i commenti
    continue;

  if (false !== ($pos = strpos($line, '='))) {
    // $str = str_replace(search, replace, subject)
    $convert[trim(substr($line, 0, $pos))] = trim(substr($line, $pos + 1));
  }
}

  foreach ($_POST as $key => $value) { //dentro ad ogni lingua faccio un lopp dei dati passati dalla form
        
    if ($key!='lng') { //se il name dell'input è diverso da lng
      $key_ok = str_replace("_", ".", $key);
      $old_string = $key_ok . " = " . $convert[$key_ok];
      $new_string = $key_ok . " = " . str_replace(array("\r\n", "\r", "\n"), "<br/>", $value);
      $res = replace_in_file($file_path, $old_string, $new_string);      
    } //fine di: "se la chiave non è lng
  }


?>