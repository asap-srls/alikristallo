<?
$pag=basename(__DIR__);
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';

?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <?
    	include '../include/header.php'; 
    ?>
    <div class="container-fluid" id="main">
	    <div class="row row-offcanvas row-offcanvas-left">
	        <? include '../include/left_nav.php'; ?>
	        <div class="col-md-9 col-lg-10 main">
	            <p class="hidden-md-up">
	                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
	            </p>

	            <h1 class="display-1 hidden-xs-down">
	             <? echo $pag ?>
	            </h1>
	            <p class="lead hidden-xs-down">Gestisci la sezione TESTI del sito</p>
	            

	            <div class="alert alert-success fade collapse" role="alert" id="myAlert">
	               
	                <strong>Tutto ok</strong> elemento modificato con successo
	            </div>
	            <div class="alert alert-danger fade collapse" role="alert" id="myAlertElimina">
	               
	                <strong>Tutto ok</strong> elemento eliminato con successo
	            </div>
	            <hr>
	            
	            
	            <div class="container-fluid">
	                <div class="row mb-3">
	                    <?
	                    	foreach ($langs as $lang_key) {
	                    		echo "<div class='col-md-3 col-6'><a href='{$root}{$pag}/nuovo.php?id={$lang_key}'>".strtoupper($lang_key)."</a></div>";
	                    	}
	                    ?>
	                </div>
	            </div>     
	            <hr>            
	        </div>
	    </div>

	</div>
    <?
    	include '../include/footer.php';
    ?>
    
  </body>
</html>