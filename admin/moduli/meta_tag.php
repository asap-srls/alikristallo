<div class="input-group col-md-6">
  <span class="input-group-addon" id="sez_title_<? echo $lang_key ?>">title</span>
  <input type="text" class="form-control" name="title_<? echo $lang_key ?>" id="title_<? echo $lang_key ?>" placeholder="inserisci qui il testo" aria-describedby="sez_title_<? echo $lang_key ?>" value="<? echo $data['title']?>">
</div>
<div class="input-group col-md-6">
  <span class="input-group-addon" id="sez_description_<? echo $lang_key ?>">description</span>
  <input type="text" class="form-control" name="description_<? echo $lang_key ?>" id="description_<? echo $lang_key ?>" placeholder="inserisci qui il testo" aria-describedby="sez_description_<? echo $lang_key ?>" value="<? echo $data['description']?>">
</div>