<div class="input-group col-12 text-center">
	<h6><? echo $type_check ?></h6>
  	<div class="checkbox">
  	<? 
  	$checks = json_decode($dbValCheck, TRUE);
  	if ($tipo=="array") {
  		foreach ($check_array as $chiave => $valore) { ?>
	  		<label style="font-size: 1.5em">
	        	<input type="checkbox" name="<? echo $name_check ?>[]" value="<? echo $chiave ?>" <? 
	        	if (is_array($checks)) {	        		        	
	        		foreach ($checks as $key => $value) {
                    echo ($key==$chiave ? 'checked' : '');
                    	}
                    }?>>
	        	<span class="cr"><i class="cr-icon fa fa-bolt"></i></span>
	        	<? echo $valore ?>
	    	</label>
  		<? }
  	}else{
  		while($checkbox = $q_check->fetch(PDO::FETCH_ASSOC)){ ?>
		 	<label style="font-size: 1.5em">
	        	<input type="checkbox" name="<? echo $name_check ?>[]" value="<? echo $checkbox['chiave'] ?>"
	        	<? 
	        	if (is_array($checks)) {	        		        	
	        		foreach ($checks as $key => $value) {
                    echo ($key==$checkbox['chiave'] ? 'checked' : '');
                    	}
                    }?>>
	        	<span class="cr"><i class="cr-icon fa fa-bolt"></i></span>
	        	<? echo $checkbox['nome'] ?>
	    	</label>
		<? }
  	}
  ?>
    	
  	</div>
</div>