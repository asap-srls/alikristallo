<?
//se la lingua non c'è inserire NULL sul campo $lingua
function alt($id_name, $numero_img, $lingua, $valori_db){
	for ($z=0; $z < $numero_img ; $z++) {  ?>
		<div class="input-group col-md-6">
			<label class="form-group has-float-label">		  
		  		<input type="text" class="form-control" name="<?= $id_name."_". $z.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name."_". $z.(isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui il testo" aria-describedby="sez_<?= $id_name."_". $z .(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valori_db[$z] ?>">
		  		<span id="sez_<?= $id_name."_". $z .(isset($lingua) ? "_".$lingua : "") ?>">alt immagine <?= $z+1 ?></span>
		  	</label>
		</div>	
	<? }
}

function text_area ($id_name, $lingua, $descrizione, $valore_db){
	?>
	<div class="col-md-12">
		<h6><?= $descrizione ?></h6>
	    <textarea name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" >
	          <?= $valore_db ?>      
	    </textarea>
	    <script>
	        var editor = CKEDITOR.replace( '<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>' );
	        editor.on( 'change', function( evt ) {
	        	$('#<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>').val(CKEDITOR.instances.<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>.getData());
	        });
	    </script>
	</div>
<? }

function checkbox ($id_name, $lingua, $descrizione, $tipologia, $data, $clausole, $fieldname_value, $fieldname_show, $valore_db, PDO $db){
	//$id_name->nome del campo
	//$lingua (se non c'è impostare NULL)
	//$descrizione->descrizione del campo, $tipologia-> se array o database
	//$data-> nome del database oppure l'array intero
	//$clausole-> le clausole per la query
	//$fieldname_value-> il valore da dare al name (il nome del campo a db)
	//$fieldname_show-> il valore che si vede fuori (il nome del campo a db)
	//$valore_db-> il valore già inserito
	?>
	<div class="col-12"><h6><?= $descrizione ?></h6></div>
	<div class="input-group col-12 text-center">
		
	  	<div class="checkbox">
	  	<? 
	  	$checks = json_decode($valore_db, TRUE);
	  	if ($tipologia=="array") {
	  		foreach ($data as $chiave => $valore) { ?>
		  		<label style="font-size: 1.5em">
		        	<input type="checkbox" name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[]" value="<?= $chiave ?>" <? 
		        	if (is_array($checks[0])) {	        		        	
		        		foreach ($checks as $key => $value) {
	                    echo ($value==$chiave ? 'checked' : '');
	                    	}
	                    }?>>
		        	<span class="cr"><i class="cr-icon fa fa-bolt"></i></span>
		        	<?= $valore ?>
		    	</label>
	  		<? }
	  	}else{
	  		$database = $data . (isset($lingua) ? "_" .$lingua : "_it");
			$q_check = $db->prepare("SELECT * FROM $database $clausole");                          
		    $q_check->execute();
	  		while($checkbox = $q_check->fetch(PDO::FETCH_ASSOC)){ 	  			
	  		?>
			 	<label style="font-size: 1.5em">
		        	<input type="checkbox" name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[]" value="<?= $checkbox[$fieldname_value] ?>"
		        	<? 
		        	if (is_array($checks)) {
		        		foreach ($checks as $key => $value) {
	                    echo ($value==$checkbox[$fieldname_value] ? 'checked' : '');
	                    	}
	                    }?>>
		        	<span class="cr"><i class="cr-icon fa fa-bolt"></i></span>
		        	<?= $checkbox[$fieldname_show] ?>
		    	</label>
			<? }
	  	}
	  ?>
	    	
	  	</div>
	</div>

<?
}

function simpleCheckbox($id_name, $lingua, $descrizione, $value, $data){ 
	// nome dell'id, lingua (se non c'è impostare NULL), label, value della checkbox, dati passati da database
	?>
	<div class="input-group col-md-6">
		<div class="checkbox">
		    <label style="font-size: 1.5em">
		        <input type="checkbox" class="simplecheck_val" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $value ?>" <?= ($data==$value ?'checked':'') ?>>
		        <span class="cr"><i class="cr-icon fa fa-bomb"></i></span>
		        <?= $descrizione ?>
		    </label>
	    </div>
	</div>
<?}

function switchCheckbox($id_name, $lingua, $descrizione, $value, $data){ 
	// nome dell'id, lingua (se non c'è impostare NULL), label, value della checkbox, dati passati da database
	?>
	<div class="col-md-4 pb-3">
		
				<h6 class="mr-3"><?= $descrizione ?></h6>
				<div class="onoffswitch">
					<input type="checkbox" class="onoffswitch-checkbox" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $value ?>" <?= ($data==$value ?'checked':'') ?>>
					<label class="onoffswitch-label" for="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $value[2] ?>">
						<span class="onoffswitch-inner"></span>
						<span class="onoffswitch-switch"></span>
					</label>
				</div>
		
	</div>
<?}

function select($id_name, $lingua, $descrizione, $tipologia, $data, $clausole, $fieldname_value, $fieldname_show, $valore_db, PDO $db){
/* nome dell'id o del name
lingua (se non c'è è NULL)
descrizione - label
se i dati provengono da database o array,
i dati se è un array oppure il nome del database
le clausole da chiamare per la chiamata a db
il nome del campo del value
il nome del campo da mostrare nella select
il valore che ha se è già stato inserito  */

	?>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
			<select class="selectpicker form-control" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" aria-describedby="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>">
			  <option value="">---scegli un'opzione---</option>
			  <? 
			  	if ($tipologia=="array") {
			  		foreach ($data as $chiave => $valore) {
			  		echo "<option value='".$chiave."' ".($valore_db==$chiave ? "selected":"").">".$valore."</option>";	
			  		}
			  	}else{
			  		$database = $data . (isset($lingua) ? "_" .$lingua : "_it");
					$q_select = $db->prepare("SELECT * FROM $database $clausole");                          
				    $q_select->execute();
			  		while($select = $q_select->fetch(PDO::FETCH_ASSOC)){ ?>
					  <option value="<?= $select[$fieldname_value] ?>" <?= ($valore_db==$select[$fieldname_value] ? "selected":"") ?>><?= $select[$fieldname_show] ?></option>
					<? }
			  	}
			  ?>
			</select>
	  		<span id="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>"><?= $descrizione ?></span>
	  	</label>
	</div>
<?
}

function select_user($id_name, $descrizione, $data, $clausole, $fieldname_value, $fieldname_show, $valore_db, PDO $db){
// nome dell'id o del name, label, il nome del database, le clausole da chiamare per la chiamata a db, il nome del campo del value, il nome dei campi da mostrare nella select (array), il valore che ha se è già stato inserito   

	?>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
			<select class="selectpicker form-control" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" aria-describedby="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>">
			  <option value="0">Tutti gli utenti</option>
			  <option value="999999" <?= ($valore_db==999999 ? "selected":"") ?>>Tutti gli utenti iscritti</option>
			  <? 
			  	
			  		$database = $data;
					$q_select = $db->prepare("SELECT * FROM $database $clausole");                          
				    $q_select->execute();
			  		while($select = $q_select->fetch(PDO::FETCH_ASSOC)){ ?>
					  	<option value="<?= $select[$fieldname_value] ?>" <?= ($valore_db==$select[$fieldname_value] ? "selected":"") ?>>
					  		<?
					  		foreach ($fieldname_show as $value) {
					  			if ($value=="email" or $value=="id") { //se scriviamo la mail mettiamo anche il trattino dopo
					  				echo $select[$value] . " - ";
					  			}else{
					  				echo $select[$value] . " ";
					  			}
					  		}
					  		?>
					  	</option>
					<? }
			  	
			  ?>
			</select>
	  		<span id="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>"><?= $descrizione ?></span>
	  	</label>
	</div>
<?
}

function input($id_name, $lingua, $descrizione, $valore_db, $type){
// nome dell'id, la lingua (se non c'è è NULL), la label, il valore preso da database, il tipo di input-> number, text, hidden, button
 ?>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
	  		<input type="<?= $type ?>" class="form-control" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" <?= ($type=='number'?'placeholder="inserisci la quantità"':'placeholder="inserisci qui il testo"') ?> aria-describedby="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valore_db ?>">
	  		<span id="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>"><?= $descrizione ?></span>
	  	</label>
	</div>
<? 
}

function input_mask($id_name, $lingua, $descrizione, $valore_db, $type, $mask, $placeholder){
// nome dell'id, la lingua (se non c'è è NULL), la label, il valore preso da database, il tipo di input-> number, text, hidden, button
 ?>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
			<input type="<?= $type ?>" class="form-control" name="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" id="<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>" data-inputmask="<?= $mask ?>" placeholder="<?= $placeholder ?>" aria-describedby="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valore_db ?>">	  		
	  		<span id="sez_<?= $id_name .(isset($lingua) ? "_".$lingua : "") ?>"><?= $descrizione ?></span>
	  	</label>
	</div>
<? 
}

function multi_input($id_name, $lingua, $descrizione, $valore_db, $numero){
	$decodifica = json_decode($valore_db, TRUE);
	// print_r($decodifica);	
	for ($i=0; $i < $numero ; $i++) {
	$z = $i+1;  ?>
	<div class="input-group col-md-6">
	  <label class="form-group has-float-label">
	  	<input type="text" class="form-control" name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[]" id="<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui il testo" aria-describedby="sez_<?= $id_name .$z.(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $decodifica[$i] ?>">
	  	<span id="sez_<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>"><?= $descrizione ." ". $z ?></span>
	  </label>
	</div>
<? 
	}
}


function multi_input_object($id_name, $lingua, $descrizione, $array_chiavi, $valore_db){
	/*$array_chiavi è un array con la chiave da usare, e con un array interno con tipo di input andiamo a trovarci e descrizione
	$array_chiavi = array(
		"shipped"=>array("checkbox","spedizione","si"), //in questo caso gli imposto "si" come value della checkbox
		"courier_name"=>array("text","nome del corriere")
		"prova"=>array("textarea", "prova text area")
	)
	*/
	echo "<div class='col-md-12 mt-3'><h3>". $descrizione."</h3></div>";
	$decodifica = json_decode($valore_db, TRUE);
	$z = 0;
	foreach ($array_chiavi as $key => $value) { //il value sarà settato a 0 per gli input, a 1 per le descrizioni
		if ($value[0]=='text') { //se è un input type text ?>
			<div class="input-group col-md-6">
			  <label class="form-group has-float-label">
			  	<input type="text" class="form-control" name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[<?= $key ?>]" id="<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui il testo" aria-describedby="sez_<?= $id_name .$z.(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $decodifica[$key] ?>">
			  	<span id="sez_<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>"><?= $value[1] ?></span>
			  </label>
			</div>
		<? }elseif ($value[0]=='checkbox') { //se è un input type checkbox ?>
			<div class="input-group col-md-6">
				<div class="container">
					<div class="row align-items-center">
						<h6 class="mr-3"><?= strtoupper($value[1]) ?></h6>
						<div class="onoffswitch">
						    <input type="checkbox" class="onoffswitch-checkbox" name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[<?= $key ?>]" id="<?= $id_name. $z .(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $value[2] ?>" <?= ($decodifica[$key]==$value[2] ?'checked':'') ?>>
						    <label class="onoffswitch-label" for="<?= $id_name. $z .(isset($lingua) ? "_".$lingua : "") ?>" value="<?= $value[2] ?>">
						        <span class="onoffswitch-inner"></span>
						        <span class="onoffswitch-switch"></span>
						    </label>
						</div>
					</div>
				</div>				
			</div>
		<? }elseif ($value[0]=='textarea') { ?>
			<div class="col-md-12">
				<h6><?= $value[1] ?></h6>
			    <textarea name="array_<?= $id_name.(isset($lingua) ? "_".$lingua : "") ?>[<?= $key ?>]" id="<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>" >
			          <?= $value[2] ?>
			    </textarea>
			    <script>
			        var editor = CKEDITOR.replace( '<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>' );
			        editor.on( 'change', function( evt ) {
			        	$('#<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>').val(CKEDITOR.instances.<?= $id_name . $z .(isset($lingua) ? "_".$lingua : "") ?>.getData());
			        });
			    </script>
			</div>
		<? }

		$z++;
	}
}


function input_titolo($lingua, $grandezza, $valore_db){
	if($grandezza == 12){
echo "<div class='col-md-12'>";
	}?>
<div class="input-group input-group-lg col-md-6">
	<label class="form-group has-float-label">
		<input type="text" class="form-control" name="titolo<?= (isset($lingua) ? "_".$lingua : "") ?>" id="titolo<?= (isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui il testo" aria-describedby="sez_titolo<?= (isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valore_db ?>">
		<span id="sez_titolo<?= (isset($lingua) ? "_".$lingua : "") ?>">Titolo <?= $lingua ?></span>
	</label>
    
</div>
<? if($grandezza == 12){
echo "</div>";
	}
}

function meta_tag($lingua, $valore_db1, $valore_db2){
	?>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
	  		<input type="text" class="form-control" name="title<?= (isset($lingua) ? "_".$lingua : "") ?>" id="title<?= (isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui il title" aria-describedby="sez_title<?= (isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valore_db1 ?>">
	  		<span id="sez_title<?= (isset($lingua) ? "_".$lingua : "") ?>">title</span>
	  	</label>
	</div>
	<div class="input-group col-md-6">
		<label class="form-group has-float-label">
	  		<input type="text" class="form-control" name="description<?= (isset($lingua) ? "_".$lingua : "") ?>" id="description<?= (isset($lingua) ? "_".$lingua : "") ?>" placeholder="inserisci qui la description" aria-describedby="sez_description<?= (isset($lingua) ? "_".$lingua : "") ?>" value="<?= $valore_db2 ?>">
	  		<span id="sez_description<?= (isset($lingua) ? "_".$lingua : "") ?>">description</span>
	  	</label>
	</div>
<?}

function images($id_name, $numero_img, $grandezza_img, $valore_db, $pathsito, $pag, $db_table, $id){
//id nome immagine, quante immagini inserire, il testo da inserire con la grandezza, il valore preso dal database (altrimenti NULL), la path del sito, la cartella dove si trovano le immagini, il nome della tabella a database, l'id dell'oggetto
	for ($i=0; $i < $numero_img; $i++) { 	
	?>

<div class="row carica-file">           
  <div class="col-3 text-center">
  <h6>Grandezza: <strong><?= $grandezza_img ?></strong></h6>
    <label for="<?= $id_name.$i ?>" class="custom-file-upload">
        <i class="fa fa-cloud-upload"></i> <?= $i+1 . ($id_name=="pdf"?"<sup>o</sup> file":"<sup>a</sup> immagine") ?>
    </label>    
    <input id="<?= $id_name.$i ?>" name="<?= $id_name . "[]"; ?>" class="img_rep" data-max-files="1" type="file" multiple/>
    <!-- <button onclick="reset($('#<?= $id_name?>'))">Cancella tutte le immagini</button> -->
  </div>
  <div class="col-9 text-center image-holder-<?= $id_name.$i ?>">
	  <? 
	  	if ($id_name=='pdf') {
	  		if ($valore_db!="" or $valore_db!=FALSE or $valore_db!=NULL) {
		  	 	$pdf = json_decode($valore_db, TRUE);
		  	 	if (is_array($pdf)) {
		  	 		foreach ($pdf as $k => $v) {
		  	 			echo "<a href='".$pathsito."img/".$pag."/".$v."' target='_blank'><img src='".$pathsito."admin/img/pdf.png' class='class-img-input'></a>";
		  	 			echo "<h6 style='margin:3px 0 10px 0'>".$v."</h6>";
		  	 		}
		  	 	}
		  	}
	  	}else{
	  		if ($valore_db!="" or $valore_db!=FALSE or $valore_db!=NULL) {  	
		  		// if ($numero_img>1) {  			
		  			$images = json_decode($valore_db, TRUE);
		  			if (is_array($images)) { //se non funziona metti $images[0]
		  				foreach ($images as $k => $v) {
		  					if ($i==$k) {
		  						if ($v!="" or $v!=NULL){
		  							echo "<img src='".$pathsito."img/".$pag."/".$v."' class='class-img-input'><a class='hover-img' data-idImage='".$id."' data-dbTable='".$db_table."' data-idObj='".$i."' data-nameObj='".$id_name."'><i class='fa fa-2x fa-trash-o'></i></a>";
		  							echo "<h6 style='margin:3px 0 10px 0'>".$v."</h6>";
		  						}
		  					}
		  				}
		  			}
		  			
		  		// }else{
		  		// 	echo "<img src='".$pathsito."img/".$pag."/".$valore_db."' class='class-img-input'>";
		  		// }
		  	}
	  	} 
 	?>                
  </div>
</div>

<? 
	} //END FOR
}
?>