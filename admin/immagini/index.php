<?
$pag=basename(__DIR__);
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';

//determino la cartella dove bisogna fare l'upload
//può essere così o generalmente la cartella immagini
$path_upload = $path ."upload/";
// $path_upload = $path;
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
  <style type="text/css">
  	.bordered{
  		border: 1px solid #c1c1c1;
	    padding: 5px;
  	}
  	.copy{
  		cursor: pointer;
  	}
  	.copied{  		
  		position: fixed;
  		top: 0;
  		left: 0;
  		width: 100%;
  		height: 100%;
  		background: #44e08c;
  		color: white;
  		display: flex;
  		align-items: center;
  		z-index: 999;
  	}
  	.copied h1{
  		margin: 0 auto;
  	}
  </style>
    
</head>
  <body >
    <?
    	include '../include/header.php'; 
    ?>
    <div class="copied" style="display: none;">
    	<h1>LINK COPIATO!</h1>
    </div>
    <div class="container-fluid" id="main">
	    <div class="row row-offcanvas row-offcanvas-left">
	        <? include '../include/left_nav.php'; ?>
	        <div class="col-md-9 col-lg-10 main">
	            <p class="hidden-md-up">
	                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
	            </p>

	            <h1 class="display-1 hidden-xs-down">
	             <? echo $pag ?>
	            </h1>
	            <p class="lead hidden-xs-down">Controlla o carica immagini dentro la cartella <strong><?= substr($path_upload, 6) ?></strong></p>
	            <div class="alert alert-success fade collapse" role="alert" id="myAlert">
	               
	                <strong>Tutto ok</strong> immagine aggiunta con successo
	            </div>
	            <hr>
	            <div class="container-fluid mb-3">
	            	<form method="post" id="fileForm" enctype="multipart/form-data">
	            		<input type="hidden" name="path" value="<?= $path_upload ?>">
		            	<div class="row carica-file">           
						  <div class="col-3 text-center">
						  	<h6>Carica una nuova immagine</h6>
						    <label for="upload_img" class="custom-file-upload">
						        <i class="fa fa-cloud-upload"></i> - max 2mb
						    </label> 
						    <input id="upload_img" name="upload_img" class="img_rep" data-max-files="1" type="file"/>
						    
						  </div>
						  <div class="col-9 text-center image-holder-upload_img"></div>
						  <div class="col-12"><span class="btn btn-danger btn-salva" id="salvaImg">SALVA</span></div>
						</div>
					</form>
	            </div>
	            
	            <div class="container-fluid">
	            	<h3 class="col-12"><i class="fa fa-folder" aria-hidden="true"></i> <?= $pathsito . substr($path_upload, 6) ?></h3>
	                <div class="row mb-3" id="imageShow">
	                	
	                    <?	                    	
							$files = scandir($path_upload);
							$files = array_diff(scandir($path_upload), array('.', '..', '.DS_Store'));
							// print_r( $files);
							foreach ($files as $file) {
								if (substr($file, -4,1)=="." or substr($file, -5,1)==".") { //se c'è il punto, quindi vuol dire che è un'immagine
									if (substr($file, -3)=="mp4") { //se è un video
										echo "<div class='col-md-3 col-6 mt-2 mb-2 text-center bordered'>
												<h6>{$file}</h6><span class='copy btn btn-danger mb-2'>COPIA LINK</span>
												<video autoplay loop style='max-width:100%'>
												  <source src='{$pathsito}".substr($path_upload, 6)."{$file}' type='video/mp4'>												  
												</video>
											</div>";
									}else{
										echo "<div class='col-md-3 col-6 mt-2 mb-2 text-center bordered'><span class='copy btn btn-danger mb-2'>COPIA LINK</span><h6>".substr($path_upload, 10)."{$file}</h6><img src='{$pathsito}".substr($path_upload, 6)."{$file}' class='img-fluid'></div>";	
									}
									
								}else{
									echo "<div class='col-md-3 col-6 mt-2 mb-2 text-center bordered'><span class='copy btn btn-danger mb-2'>COPIA LINK</span><h6>".substr($path_upload, 10)."{$file}/</h6><h2><i class='fa fa-folder fa-3x' aria-hidden='true'></i></h2></div>";
								}
							}

	                    	// foreach ($langs as $lang_key) {
	                    	// 	echo "<div class='col-md-3 col-6'><a href='{$root}{$pag}/nuovo.php?id={$lang_key}'>".strtoupper($lang_key)."</a></div>";
	                    	// }
	                    ?>
	                </div>
	            </div>     
	            <hr>            
	        </div>
	    </div>

	</div>
    <?
    	include '../include/footer.php';
    ?>
    <script type="text/javascript">
    	function copyToClipboard(element) {
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($(element).text()).select();
			document.execCommand("copy");
		  	$temp.remove();
		  	$('.copied').removeClass('collapse , fade').fadeIn( 500 );
			setTimeout(function() {
                $('.copied').addClass('collapse , fade').fadeOut( 500 );
            }, 2000);
		}
    	$(document).ready(function(){
    		$('#salvaImg').on('click', function(){
    			var fileForm = $("#fileForm")[0];
                var strFile = new FormData(fileForm);

                $.ajax({
	                type: "POST",
	                url: "insert_file.php",
	                data: strFile,              
	                contentType: false,
	                processData: false,
	                success: function(msg)
	                {
	                  $('#myAlert').removeClass('collapse , fade').fadeIn( 1000 );
	                      setTimeout(function() {
	                          $('#myAlert').addClass('collapse , fade').fadeOut( 1000 );
	                      }, 4000);
	                  var toPrepend = "<div class='col-md-3 col-6 mt-2 mb-2 text-center bordered'><span class='copy btn btn-danger mb-2'>COPIA LINK</span><h6><?= substr($path_upload, 10) ?>"+msg+"</h6><img src='<?= $pathsito . substr($path_upload, 6)?>"+msg+"' class='img-fluid'></div>";
	                  $('#imageShow').prepend(toPrepend);
	                },
	                error: function()
	                {
	                  alert("errore nella seconda chiamata"); //sempre meglio impostare una callback in caso di fallimento
	                }
    			});
            });

    		$('.copy').on('click', function(){
    			var elem = $(this).closest('div').find('h6');
    			copyToClipboard(elem);
    		});

    	});
    	$(document).ajaxComplete(function(){
   			$('.copy').on('click', function(){
   			var elem = $(this).closest('div').find('h6');
    		copyToClipboard(elem);
    		});
    	});
    </script>
    
  </body>
</html>