<?php
$pag="index";
include('include/config.php');
if(isset($_SESSION['name'])){
  $admin_name = $_SESSION['name'];
  $q_admin = $db->prepare("SELECT * FROM login WHERE user=$admin_name LIMIT 1");
  $q_admin -> execute();
  $count = $q_admin->rowCount();
  if ($count>0) {
    header("Location:intro.php");
  }
}
if (isset($_GET['result'])) {
  $result = $_GET['result'];
}

?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include 'include/head.php' ?>
    
  </head>
  <body >
<form name="myform" id="contactForm" action="<? echo $root ?>verifica.php" method="post">     
  <div class="container-fluid" style="height:100vh">
      <div class="row align-items-center" style="height:100%;">
          <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-8 text-center card shadow-card">
              <div class="row interno-card text-center">
                <h1>ACCEDI AL PANNELLO DI CONTROLLO</h1>
                  <?
                  if ($result=='no') {
                    echo "<h5 class='col-md-12 text-center'>USER O PASSWORD NON CORRETTI</h5>";
                  }elseif ($result=='hack') {
                    echo "<h5 class='col-md-12 text-center'>STAI CERCANDO DI ENTRARE IN UN'AREA PROTETTA</h5>";
                  }elseif ($result=='data') {
                    echo "<h5 class='col-md-12 text-center'>PER FAVORE, REINSERISCI I DATI</h5>";
                  }elseif ($result=='logout') {
                    echo "<h5 class='col-md-12 text-center'>HAI ESEGUITO CON SUCCESSO IL LOGOUT</h5>";
                  }
                  ?>
                  <div class="col-md-6">
                      <div class="input-group input-group-lg">
                        <label class="form-group has-float-label">
                          <input type="text" name="u_name" id="u_name" class="form-control" placeholder="Username" aria-describedby="sizing-addon1" required>
                          <span>user</span>
                        </label>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="input-group input-group-lg">
                        <label class="form-group has-float-label">
                          <input type="password" name="pass" id="pass" class="form-control" placeholder="password" aria-describedby="sizing-addon1" required>
                          <span>pass</span>
                        </label>
                      </div>
                  </div>
                  <div class="form-check col-md-6 text-center">
                      <div class="checkbox">
                          <label style="font-size: 1.5em">
                              <input type="checkbox" name="remember" id="remember" value="si">
                              <span class="cr"><i class="cr-icon fa fa-rocket"></i></span>
                              Ricordami
                          </label>
                      </div>
                  </div>
                  <div class="col-md-6 text-center">
                      <button type="submit" id="entra" class="btn red-bg btn-block">ENTRA <i class="fa fa-chevron-right fa-lg"></i></a>
                  </div>

              </div>
          </div>
      </div>
  </div>
</form>
    <? 
include 'include/footer.php'; 
// chiudo la connessione a MySQL
  $db = null;
?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#entra").on('click', function(){
    
      if ($('#remember').is(':checked')==true) {
        var user = $('#u_name').val();
        var pass = $('#pass').val();
        localStorage.setItem( 'user', user );
        localStorage.setItem( 'pass', pass );
        localStorage.setItem('remember', 'ok');
        // $('input').each(function(){
        //     localStorage.setItem( $(this).attr('id'), $(this).val() );
        // });
      }else{
        localStorage.setItem( 'user', '' );
        localStorage.setItem( 'pass', '' );
        localStorage.setItem('remember', 'no');
      }
    });

    if (localStorage.getItem('remember')=='ok') {
      $('#remember').attr('checked', true);
      var localUser = localStorage.getItem('user');
      var localPass = localStorage.getItem('pass');
      $('#u_name').val(localUser);
      $('#pass').val(localPass);
    }
  });
</script>
  </body>
</html>