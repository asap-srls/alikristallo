$(document).ready(function() {    
    // var path = (location.protocol+"//"+location.host+"/admin");
    
//codice per nascondere i nomi delle categorie al focus dell'input
    // $( "input[type=text]" ).focusin(function() {        
    //   $(this).closest('div').find("span").toggle('slide');      
    // });
    // $( "input[type=text]" ).focusout(function() {        
    //   $(this).closest('div').find("span").toggle('slide');      
    // });

  $('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
  });
  //end
  
  $("input[type=file]").on('change', function () {
    var $input = $(this);
    //prendo l'id della input
    var input_id = $input.attr('id');
    // e lo trasformo nella classe per inserire l'immagine
    var imageHolder = ".image-holder-" + input_id; 
    //vedo quanti file massimo si possono inserire
    var maxFiles = $(this).attr("data-max-files");

    var maxSize = 2097152; //2mb    

     //Conto i file inseriti
     var countFiles = $(this)[0].files.length;
     if (countFiles<=maxFiles) {

      if ($(this)[0].files[0].size<maxSize) {

        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $input.closest(".carica-file").find(imageHolder);
        image_holder.empty();

        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof (FileReader) != "undefined") {

                 //loop per ogni file inserito
                for (var i = 0; i < countFiles; i++) {

                     var reader = new FileReader();
                     reader.onload = function (e) {
                         $("<img />", {
                             "src": e.target.result,
                                 "class": "class-img-input"
                         }).appendTo(image_holder);
                    }

                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }

            } else {
                 alert("Questo browser non supporta il FileReader.");
          }
        } else if (extn == "pdf") {
          $("<img src='"+path+"/img/pdf.png' class='class-img-input' />").appendTo(image_holder);
        } else {
          alert("Per piacere seleziona solo immagini con estensioni: .jpg, .jpeg, .png, .gif");
        }
      }else{
        alert("le dimensioni del file devono essere al massimo di 2mb");
      }

    } else {
      alert("inserisci al massimo " + maxFiles + " immagini");
    }
 });

  //ELIMINARE UN'IMMAGINE
  $('.hover-img').on('click', function(){
    var id_element = $(this).attr("data-idImage");
    var table_element = $(this).attr("data-dbTable");
    var key_element = $(this).attr("data-idObj");
    var name_element = $(this).attr("data-nameObj");
    var divVicino = $(this).closest(".col-9");
    $.ajax({    
      type: "POST",  
      url: path+"/functions/elimina_immagine.php",    
      data: {
        id_element: id_element,
        table_element: table_element,
        key_element: key_element,
        name_element: name_element,
      },
      dataType: "html",
      success: function(msg)
      {
        divVicino.html(msg);
        alert("elemento eliminato");
      },
      error: function()
      {
      alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  })
  //ELIMINARE UN ELEMENTO
  $('.eliminaDato').on('click', function(){
                          
            var id_db = $(this).closest('tr').attr('id');
            var tabella_db = $(this).data('tabella_db');
            $.ajax({    
                    type: "POST",  
                    url: path+"/functions/elimina.php",    
                    data: {
                      id_db: id_db,
                      tabella_db: tabella_db,                     
                    },
                    dataType: "html",
                    success: function()
                    {
                      $('#'+id_db).hide("slow");
                    $('#myAlertElimina').removeClass('collapse , fade').fadeIn( 1000 );
                      setTimeout(function() {
                          $('#myAlertElimina').addClass('collapse , fade').fadeOut( 1000 );
                      }, 4000)        
                    },
                    error: function()
                    {
                    alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                    }
                  });

         });

  //MODIFICARE SE L'ELEMENTO è O NO ONLINE

  $('.splash_pubb').on('click', function(){
          if ($(this).is(":checked"))
            {
              var checked = $(this).val();
            } else {
              var checked = "";
            }
          
          var check_id = $(this).closest('tr').attr('id');
          var database = $(this).data("datab");
          var campo = $(this).data("campo");
          
            $.ajax({    
              type: "POST",  
              url: path+"/functions/online.php",    
              data: {
                checked: checked,
                check_id: check_id,
                database: database,
                campo:campo
              },
              dataType: "html",
              success: function()
              {
              $('#myAlert').removeClass('collapse , fade').fadeIn( 1000 );
                setTimeout(function() {
                    $('#myAlert').addClass('collapse , fade').fadeOut( 1000 );
                }, 4000)        
              },
              error: function()
              {
              alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
              }
            });
         });

  //FAR APPARIRE UNA FINESTRA MODALE CON I CONTENUTI
  $('#modalInput').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget); // Button that triggered the modal
          var titolo = button.data('titolo'); // Extract info from data-* attributes
          var tipologia = button.data('tipologia');
          var azione = button.data('azione');
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this);
            modal.find('.modal-title').text(titolo);
          $.ajax({    
            type: "POST",
            url: path+"/moduli/modals.php",  
            data: {
                tipologia: tipologia,
                azione: azione,
            },
            dataType: "html",
            success: function(msg)
            {            
            modal.find('.contenuto_ajax').html(msg);
            },
            error: function()
            {
            alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
            }
          });
        });


  //SORT DEI CONTENUTI -> ATTENZIONI, HO DOVUTO AGGIUNGERE LA LIBRERIA JQUERY UI PER FAR FUNZIONARE LA FUNZIONE SORTABLE

  $("#sortThis").sortable({
        
        change: function( event, ui )
        {
          $('#salvaCambiamenti').show();
        }
    });

    $('#salvaCambiamenti').on('click', function(){
        var order = $('#sortThis').sortable('toArray');
          var tipologia = $('#tipologia_selezionata').val();
          $.ajax({
                type: "GET",
                url: path+"/functions/sort.php?ids="+order+"&tipologia="+tipologia,
                
                dataType: "html",            
                success: function()
                {            
                $('#myAlert').removeClass('collapse , fade').fadeIn( 1000 );
                setTimeout(function() {
                    $('#myAlert').addClass('collapse , fade').fadeOut( 1000 );
                }, 4000);
                $('#salvaCambiamenti').hide();
                },
                error: function()
                {
                alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                }
            });                   
          
    });
 
});



$(document).ajaxComplete(function () {
  
    // $( "input[type=text]" ).focusin(function() {        
    //   $(this).closest('div').find("span").toggle('slide');      
    // });
    // $( "input[type=text]" ).focusout(function() {        
    //   $(this).closest('div').find("span").toggle('slide');      
    // });
    
    $("#formPop input[type=file]").on('change', function () {
    var $input = $(this);
    //prendo l'id della input
    var input_id = $input.attr('id');
    // e lo trasformo nella classe per inserire l'immagine
    var imageHolder = ".image-holder-" + input_id; 
    //vedo quanti file massimo si possono inserire
    var maxFiles = $(this).attr("data-max-files");

    var maxSize = 2097152; //2mb

     //Conto i file inseriti
     var countFiles = $(this)[0].files.length;
     if (countFiles<=maxFiles) {
      if ($(this)[0].files[0].size<maxSize) {

       var imgPath = $(this)[0].value;
       var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
       var image_holder = $input.closest(".carica-file").find(imageHolder);
       image_holder.empty();

       if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {

             //loop per ogni file inserito
             for (var i = 0; i < countFiles; i++) {

               var reader = new FileReader();
               reader.onload = function (e) {
                 $("<img />", {
                   "src": e.target.result,
                   "class": "class-img-input"
                 }).appendTo(image_holder);
               }

               image_holder.show();
               reader.readAsDataURL($(this)[0].files[i]);
             }

           } else {
             alert("Questo browser non supporta il FileReader.");
           }
         } else if (extn == "pdf") {
          $("<img src='"+path+"/img/pdf.png' class='class-img-input' />").appendTo(image_holder);
        } else {
         alert("Per piacere seleziona solo immagini con estensioni: .jpg, .jpeg, .png, .gif");
       }
      }else{
        alert("le dimensioni del file devono essere al massimo di 2mb");
      }
    } else {
      alert("inserisci al massimo " + maxFiles + " immagini");
    }
 });
    //IMPOSTARE L'ID DI UN CAMPO
    var id_cont = $('#id').val();
            if (id_cont=='' || id_cont == null) {
              var imposta_id = $("#id_impostato").val();
              $('#id').val(imposta_id);
            }

            //SALVARE UN CONTENUTO SU UNA FINESTRA MODALE A DATABASE

              $(document).on('click', '#modalAction', function(e) {
              e.preventDefault();          
                  var form = $("#formPop")[0];
                  var str = new FormData(form);                
                  //chiamata ajax
                  $.ajax({    
                  type: "POST",
                  url: path+"/functions/insert_db.php",    
                  data: str,
                  contentType: false,
                  processData: false,
                  success: function()
                  {
                  // qui fai comparire l'alert che ho salvato sul file modals.php
                  $('#myModalAlert').removeClass("fade");
                  $('.modal-form').fadeOut();
                  $('#myModalAlert').fadeIn();                  
                  },
                  error: function()
                  {
                  alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                  }
                });
                  e.stopImmediatePropagation();
                return false;
              });

        //AGGIUNGERE O MODIFICARE UN CONTENUTO DA uNA FINESTRA MODALE
        $(document).on('click', '.modifica_contenuto', function (e) {
        e.preventDefault();       
            var azione = $(this).data("azione");
            var tipologia = $(this).data("tipologia");
            var obj_id = $(this).data("obj_id");
            var titolo = $(this).data("titolo");
            var modal = $('#modalInput');
                modal.find('.modal-title').text(titolo);
            $.ajax({    
                type: "POST",
                url: path+"/moduli/modals.php",
                data: {
                    tipologia: tipologia,
                    azione: azione,
                    obj_id: obj_id,
                },
                dataType: "html",            
                success: function(msg)
                {            
                modal.find('.contenuto_ajax').html(msg);
                },
                error: function()
                {
                alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                }
            });

        });

        //FAR APPARIRE O NO UN ELEMENTO ON-LINE
        $('.modal_pubb').on('click', function(e){
              
                if ($(this).is(":checked"))
                  {
                    var checked = $(this).val();
                  } else {
                    var checked = "";
                  }
                
                var check_id = $(this).closest('tr').attr('id');
                var categoria_oggetto = $(this).data("categoria-oggetto");
                
                  $.ajax({    
                    type: "POST",  
                    url: path+"/functions/online.php",    
                    data: {
                      checked: checked,
                      check_id: check_id,
                      categoria_oggetto: categoria_oggetto,
                    },
                    dataType: "html",
                    success: function()
                    {
                    $('#myAlertModal').removeClass('collapse , fade').fadeIn( 1000 );
                      setTimeout(function() {
                          $('#myAlertModal').addClass('collapse , fade').fadeOut( 1000 );
                      }, 4000)        
                    },
                    error: function()
                    {
                    alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                    }
                  });
                
               });

        $('.splash_pubb').on('click', function(){
          if ($(this).is(":checked"))
            {
              var checked = $(this).val();
            } else {
              var checked = "";
            }
          
          var check_id = $(this).closest('tr').attr('id');
          var database = $(this).data("datab");
          var campo = $(this).data("campo");
          
            $.ajax({    
              type: "POST",  
              url: path+"/functions/online.php",    
              data: {
                checked: checked,
                check_id: check_id,
                database: database,
                campo:campo
              },
              dataType: "html",
              success: function()
              {
              $('#myAlert').removeClass('collapse , fade').fadeIn( 1000 );
                setTimeout(function() {
                    $('#myAlert').addClass('collapse , fade').fadeOut( 1000 );
                }, 4000)        
              },
              error: function()
              {
              alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
              }
            });
         });

        //ELIMINARE UN CONTENUTO DALLA FINESTRA MODALE
        $('.eliminaDatoModal').on('click', function(){
                          
        var id_db = $(this).closest('tr').attr('id');
        var tr_selected = $(this).closest('tr');
        var tabella_db = $(this).data('tabella_db');
        $.ajax({    
                type: "POST",  
                url: path+"/functions/elimina.php",    
                data: {
                    id_db: id_db,
                    tabella_db: tabella_db,                     
                },
                dataType: "html",
                success: function()
                {
                 tr_selected.hide("slow");
                $('#myAlertEliminaModal').removeClass('collapse , fade').fadeIn( 1000 );
                    setTimeout(function() {
                        $('#myAlertEliminaModal').addClass('collapse , fade').fadeOut( 1000 );
                    }, 4000)        
                },
                error: function()
                {
                alert("errore nella chiamata"); //sempre meglio impostare una callback in caso di fallimento
                }
                
        });
    });

        //BUG FIX CKEDITOR ON MODAL BOOTSTRAP
        // $.fn.modal.Constructor.prototype._enforceFocus = function() {
        //         var $modalElement = this.$element;
        //         $(document).on('focusin.modal',function(e) {
        //             if ($modalElement[0] !== e.target
        //                 && !$modalElement.has(e.target).length
        //                 && $(e.target).parentsUntil('*[role="dialog"]').length === 0) {
        //                 $modalElement.focus();
        //             }
        //         });
        //     };

        $.fn.modal.Constructor.prototype.enforceFocus = function () {
              modal_this = this
              $(document).on('focusin.modal', function (e) {
                  if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                  // add whatever conditions you need here:
                  &&
                  !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                      modal_this.$element.focus()
                  }
              })
          };

  });

