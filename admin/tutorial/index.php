<?
include('../include/config.php');
include '../functions/session.php';
$pag="tutorial";
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <!--/col-->

        <div class="col-md-9 col-lg-10 main">

            <!--toggle sidebar button-->
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>

            <h1 class="display-1 hidden-xs-down">
            <!-- <svg class="svg-icon">
                <use xlink:href="#team" />
            </svg> --> Inserimento
            </h1>
            <p class="lead hidden-xs-down">Scopri come inserire un contenuto nell'area amministrativa</p>


            <hr>

            <div class="row placeholders mb-3">
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/dddddd/fff?text=1" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Nuovo contenuto</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page, e sul menù a sinistra clicca sulla voce <i class="red">"NUOVO CONTENUTO"</i></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e4e4e4/fff?text=2" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Campi di inserimento</h4>
                            <span class="text-muted">
                            Ogni campo ha una descrizione della sua funzione nel sito.</br>
                            I campi <i class="red">title</i> e <i class="red">description</i> sono i campi utili alla SEO del sito (sono quelli letti dai motori di ricerca).</br>
                            Ci sono 4 tipi di campi di inserimento:</br>
                            <ul>
                                <li><i class="red">input</i>: sono i campi su una sola riga, dove i testi di solito devono essere di dimensioni brevi (max 200 caratteri)</li>
                                <li><i class="red">text</i>: sono i campi di inserimento di testi lunghi, di solito usati per inserire le descrizioni del contenuto. Non hanno limiti</li>
                                <li><i class="red">select</i>: sono i campi di selezione. Puoi selezionare un elemento solo per ogni campo. Sono i famosi menù a "tendina".</li>
                                <li><i class="red">checkbox</i>: sono i campi di selezione multipla. Servono per selezionare più elementi.</li>
                                <li><i class="red">file</i>: sono i campi di inserimento di file o immagini. In ogni campo c'è la descrizione di cosa bisogna inserire (file o immagine), il peso e la grandezza massima.</li>
                            </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/d6d6d6/fff?text=3" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Salvataggio</h4>
                            <span class="text-muted">Cliccando sul tasto <i class="red">SALVA</i> tutti i dati inseriti verranno salvati a database, si aprirà una finestra in pop up che confermerà il salvataggio</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e0e0e0/fff?text=4" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Pop Up</h4>
                            <span class="text-muted">
                                Nella finestra pop-up potrai decidere se:<br>
                                <ul>
                                    <li>chiuderla - tornare alla pagina dell'elemento e modificare i campi inseriti</li>
                                    <li>passare alla pagina di  di tutti gli elementi presenti a database</li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<? include '../include/footer.php'; ?>
  </body>
</html>