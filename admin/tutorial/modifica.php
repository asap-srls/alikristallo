<?
include('../include/config.php');
include '../functions/session.php';
$pag="tutorial";
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <!--/col-->

        <div class="col-md-9 col-lg-10 main">

            <!--toggle sidebar button-->
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>

            <h1 class="display-1 hidden-xs-down">
            <!-- <svg class="svg-icon">
                <use xlink:href="#team" />
            </svg> --> Modifica e elimina
            </h1>
            <p class="lead hidden-xs-down">Scopri come modificare un contenuto nell'area amministrativa</p>


            <hr>

            <div class="row placeholders mb-3">
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/dddddd/fff?text=1" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Modifica contenuto</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page, e sulla tabella di tutti i contenuti menù a sinistra clicca sull'icona di modifica</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e4e4e4/fff?text=2" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Elimina contenuto</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page, e sulla tabella di tutti i contenuti menù a sinistra clicca sull'icona di eliminazione<br><strong>Una volta cliccata quell'icona l'elemento sarà ELIMINATO PER SEMPRE, e l'unica maniera per ristabilirlo è creare un nuovo contenuto da capo</strong></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/d6d6d6/fff?text=3" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Pagina Modifica</h4>
                            <span class="text-muted">Funziona come la pagina di inserimento di un contenuto, con la differenza che si vedranno i campi che avevi già inserito a database, quei campi si possono modificare, lasciare invariati o rifarli da capo.</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e0e0e0/fff?text=4" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Salva e Pop Up</h4>
                            <span class="text-muted">
                                Cliccando su <i class="red">"SALVA"</i> salverai le modifiche a database e si aprirà una finestra POP-UP per confermare l'esito positivo dell'operazione.<br>
                                Nella finestra POP-UP potrai decidere se:<br>
                                <ul>
                                    <li>chiuderla - tornare alla pagina dell'elemento e modificare i campi inseriti</li>
                                    <li>passare alla pagina di  di tutti gli elementi presenti a database</li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<? include '../include/footer.php'; ?>
  </body>
</html>