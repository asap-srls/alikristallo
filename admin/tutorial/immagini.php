<?
include('../include/config.php');
include '../functions/session.php';
$pag="tutorial";
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <!--/col-->

        <div class="col-md-9 col-lg-10 main">

            <!--toggle sidebar button-->
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>

            <h1 class="display-1 hidden-xs-down">
            <!-- <svg class="svg-icon">
                <use xlink:href="#team" />
            </svg> --> Immagini
            </h1>
            <p class="lead hidden-xs-down">L'iserimento delle immagini nell'area amministrativa</p>


            <hr>

            <div class="row placeholders mb-3">
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/dddddd/fff?text=1" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Campo file</h4>
                            <span class="text-muted">Nel campo file, se è settato come <i class="red">Inserimento immagine</i>, <strong>RISPETTARE</strong> le indicazioni che vengono date.</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e4e4e4/fff?text=2" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Grandezza, peso è quantità</h4>
                            <span class="text-muted">                            
                            <ul>
                                <li><i class="red">Grandezza</i>: Espressa in pixel (px), sono le dimensioni che deve avere ogni singola immagine da inserire per l'ottimizzazione perfetta con la piattaforma Front-end (larghezza x altezza)</li>
                                <li><i class="red">Peso</i>: espresso in Megabite (Mb), è il peso massimo che deve avere ogni singola immagine. Se maggiore non verrà caricata.</li>
                                <li><i class="red">Quantità</i>: Sono i numeri di immagini massime che si possono inserire in un'unico campo insert di file</li>                                
                            </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/d6d6d6/fff?text=3" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Selezione delle immagini</h4>
                            <span class="text-muted">
                            Cliccando sul tasto di input file si aprirà una finestra dove si potranno selzionare al massimo la quantità di immagini richieste dal programma.<br>
                            Es: l'inserimento richiede 3 immagini? selezionare da una a 3 immagini quando si apre la finestra.<br>
                            Se si seleziona un'immagine, si chiude la finestra, e la si riapre per selezionare immagini aggiuntive non funziona. Le immagini aggiuntive selezionate sovrascriveranno quella selezionata in precedenza
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e0e0e0/fff?text=4" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Cambio</h4>
                            <span class="text-muted">
                                Nella stessa maniera dell'inserimento, per cambiare immagine/i, basta ricliccare il tasto di input file, e selezionare le nuove immagini, che sovrascriveranno le vecchie.<br>
                                Anche nel server verranno sovrascritte le vecchie immagini se hanno lo stesso nome di file (se avevo già caricato un'immagine chiamata "file.jpg" e ricarico una nuova immagine chiamata "file.jpg", quest'ultima immagine sovrascriverà la precendente nel server).
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<? include '../include/footer.php'; ?>
  </body>
</html>