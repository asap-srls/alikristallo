<?
include('../include/config.php');
include '../functions/session.php';
$pag="tutorial";
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <!--/col-->

        <div class="col-md-9 col-lg-10 main">

            <!--toggle sidebar button-->
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>

            <h1 class="display-1 hidden-xs-down">
            <!-- <svg class="svg-icon">
                <use xlink:href="#team" />
            </svg> --> Gestione
            </h1>
            <p class="lead hidden-xs-down">Scopri come gestire i contenuti nell'area amministrativa</p>


            <hr>

            <div class="row placeholders mb-3">
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/dddddd/fff?text=1" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                        <h4>Tutti i contenuti</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page</span>                            
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e4e4e4/fff?text=2" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Elimina contenuto</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page, e sulla tabella di tutti i contenuti menù a sinistra clicca sull'icona di eliminazione<br><strong>Una volta cliccata quell'icona l'elemento sarà ELIMINATO PER SEMPRE, e l'unica maniera per ristabilirlo è creare un nuovo contenuto da capo</strong></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/d6d6d6/fff?text=3" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Modifica contenuto</h4>
                            <span class="text-muted">Entra nell'area scelta cliccando sul menù oppure dalla fast dashboard in home page, e sulla tabella di tutti i contenuti menù a sinistra clicca sull'icona di modifica</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/e0e0e0/fff?text=4" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>Posizione di un contenuto</h4>
                            <span class="text-muted">Puoi spostare un contenuto tenendo cliccato su di esso e trascinandolo. L'ordine che vedrai di questi contenuti sarà quella visibile nella front-end</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 placeholder">
                    <div class="row">
                        <div class="col-3"><img src="//placehold.it/200/afafaf/fff?text=5" class="center-block img-fluid rounded-circle" alt="Generic placeholder thumbnail"></div>
                        <div class="col-9">
                            <h4>On-line</h4>
                            <span class="text-muted">Cliccando sulla checkbox <i class="red">on-line</i> potrai decidere se far vedere o no un contenuto on-line</span>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<? include '../include/footer.php'; ?>
  </body>
</html>