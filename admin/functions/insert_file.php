<?
include_once '../include/config.php';
include '../include/languages.php';
include 'functions.php';

$pag = $_POST['pag']; //prendo nome della cartella su cui verranno inserite le immagini
$id = $_POST['id']; //e id
$table_db = $_POST['db_name'];
//imposto il nuovo path, basato sulla cartella immagini di quella pagina
$new_path = $path . $pag;

if(!is_dir($new_path)) {//se non c'è la cartella la creo
	mkdir($new_path , 0777);
}

$dbTable_it = $table_db."_it";
$query = $db->prepare("SELECT * FROM $dbTable_it WHERE id = '$id'");
$query->execute();
$data = $query->fetch(PDO::FETCH_ASSOC);


foreach ($_FILES as $key=>$file) { //loop tra i vari campi di file da inserire
	$ok_invia = FALSE; //imposto sempre il FALSE all'inizio di ogni ciclo
	if ($key!='id' || $key!='pag') { //se il name dell'input è differente da 'id' o da 'pag'
		$immagini = json_decode($data[$key], TRUE); //decodifico dal database il campo immagine
		$totale_file = count($_FILES[$key]['name']); //faccio una conta sull'array delle immagini che mi arriva

		for ($i=0; $i < $totale_file ; $i++) { //ciclo for per ogni immagine dell'array di ogni 'name' che mi è arrivato
			$check_file_size = $_FILES[$key]['size'][$i];//controllo che ci siano state inserite le immagini
			$check_file_error = $_FILES[$key]['error'][$i];

	    	if (is_uploaded_file($_FILES[$key]['tmp_name'][$i])) {
				$tmpFilePath = $_FILES[$key]['tmp_name'][$i];
				$nameFile = $_FILES[$key]['name'][$i];
				//assicuriamoci di avere una filepath
				if ($tmpFilePath != ""){
					//Facciamo il setup della nuova path del file
					$newFilePath = $new_path . "/" . $nameFile;

					//controlliamo se il file esiste già nella cartella, se esiste lo rimuoviamo per rimpiazzarlo
					//con il nuovo file con lo stesso nome
					if(file_exists($newFilePath)) unlink($newFilePath);

					//facciamo l'upload del file
					if(move_uploaded_file($tmpFilePath, $newFilePath)){					
						if($check_file_size!=0 && $check_file_error!=4){
	    					
	    					$ok_invia = TRUE;//imposto a TRUE la variabile per inviare a database i cambiamenti    					
							$immagini[$i] = $nameFile; //cambio il nome dell'immagine al posto di quella già esistente
							$invio_immagini = json_encode($immagini); //codifico il database con i nomi immagine					
						}
					}
				}
			}
		}
		
	  	if($ok_invia != FALSE){ //se la variabile è settata in TRUE allora vuol dire che c'è stato un cambiamento e possiamo caricare i cambiamenti
	    	foreach ($langs as $k) { 
				$dbTable = $table_db."_".$k;
				$q_up_img = $db->prepare("UPDATE $dbTable SET $key='$invio_immagini' WHERE id='$id'");
				$q_up_img->execute();
			}
	  	}
	}
}

echo "Tutto ok! elemento salvato nel database";
// chiudo la connessione a MySQL
	$db = null;
	exit;
?>