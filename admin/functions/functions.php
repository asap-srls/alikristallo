<?
//funzione per l'inserimento della checkbox
function isCheckbox($data){
  // $array = array($data);
  // $count = count($array);
  // if ($count>1) {
  // $value = json_encode($array);
  // }else{
  //   $value = $data;
  // }
  $value = json_encode($data);
  return $value;
}

//funzione per l'inserimento delle immagini
function saveImages($img, $new_path){
  $totale_file =  count($img['name']);
  $immagini = array();
  
  for($i=0; $i<$totale_file; $i++) {    
    if (is_uploaded_file($img['tmp_name'][$i])) {
      $tmpFilePath = $img['tmp_name'][$i];
      $nameFile = $img['name'][$i];      
      if ($tmpFilePath != ""){
      //Facciamo il setup della nuova path del file
      $newFilePath = $new_path . "/" . $nameFile;

      //controlliamo se il file esiste già nella cartella, se esiste lo rimuoviamo per rimpiazzarlo
      //con il nuovo file con lo stesso nome
      if(file_exists($newFilePath)) unlink($newFilePath);

      //facciamo l'upload del file
      if(move_uploaded_file($tmpFilePath, $newFilePath)){         
          if ($totale_file>1) {
            $immagini[] = $nameFile;
            $invio_immagini = json_encode($immagini);
          }else{
            $invio_immagini = $nameFile;        
            }
          }
      }
    }
  }
  return $invio_immagini;
}

//funzione che viene chiamata nel caso l'id non sia impostato
// function createNewRow($db, $dbtable){
//   $q_in = $db->prepare("INSERT INTO $dbTable (pubblica) VALUES ('si')"); //devo provare a inserire una riga vuota, senza impostare nessun campo, se non l'id
//   $q_in->execute();
//   $id = $db->lastInsertId();
//   return $id;
// }
?>