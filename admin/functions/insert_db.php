<?
include_once '../include/config.php';
include '../include/languages.php';
include 'functions.php';

$id = $_POST['id'];
$pag = $_POST['pag'];
if (!empty($_POST['pubblica'])&&$_POST['pubblica']!="no") {
  $pubblica = $_POST['pubblica'];
}else{
  $pubblica = NULL;
}
$dbname = $_POST['db_name'];

$dbTable_it = $dbname."_it";
$q_id = $db->prepare("SELECT * FROM $dbTable_it ORDER BY id DESC LIMIT 1");
$q_id->execute();
$data_id = $q_id->fetch(PDO::FETCH_ASSOC);
$lastId= $data_id['id'];

$new_path = $path . $pag;

$condizioni = "";

$i = 0; //variabile i-> mi serve per capire se sono alla prima condizione inserita o meno
foreach ($langs as $k) { //faccio un loop delle lingue

  $database_name = $dbname."_".$k; //setto il nome del database in base alla lingua

  if ($id>$lastId) { //Se l'id è maggiore dell'ultimo id inserito a database allora creo un nuovo inserimento
    $q_in = $db->prepare("INSERT INTO $database_name () VALUES ()");
    $q_in->execute();
  }
  if (isset($pubblica)) { //se effettivamente esiste la checkbox "pubblica on line" allora lo setto per ogni lingua 
    $q_up0 = $db->prepare("UPDATE $database_name SET pubblica='$pubblica' WHERE id='$id'");
    $q_up0->execute(); 
  }
  
  foreach ($_POST as $key => $value) { //dentro ad ogni lingua faccio un lopp dei dati passati dalla form
        
    if ($key!='id' || $key!='pag' || $key!='db_name' || $key!='pubblica') { //se il name dell'input è diverso da questi valori allora procedo
      
      if (in_array(substr($key, -2), $langs)) { //se il name dell'input è nell'array delle lingue, quindi vuol dire che siamo in lingua
                    
        if (strpos(substr($key, -2), $k) !== false) { //controllo che il valore della chiave sia quello della lingua
          // $i++;
          $array_suffix = "array_";
          $lunghezza_array_suffix = strlen($array_suffix);
          if(strpos($key, $array_suffix)!==FALSE){
            $value = isCheckbox($value);
            $newkey = substr($key, $lunghezza_array_suffix, -3);//riscrivo il nome della chiave levando via la lingua e la parte iniziale-> es: array_nome_it diventa nome
          }else{
            $value = trim(addslashes($value));
            $newkey = substr($key, 0, -3); //riscrivo il nome della chiave levando via la lingua-> es: nome_it diventa nome
          }
          // if (count($key)>1){          
          //   $value = isCheckbox($value); //con questa funzione controllo se è una checkbox o meno
          // }
          // $newkey = substr($key, 0, -3); //riscrivo il nome della chiave levando via la lingua-> es: nome_it diventa nome
          $q_up = $db->prepare("UPDATE $database_name SET $newkey = '$value' WHERE id='$id'"); //faccio l'update delle condizioni a database
          $q_up->execute();
          // $condizioni .= ($i>1? ", ":"").$newkey."='".$value."'";//scrivo la nuova riga di condizioni
        }
      }else{ //se invece non trovo che le ultime due lettere di un name siano dentro l'array delle lingue
        // $i++;
        $array_suffix = "array_";
        $lunghezza_array_suffix = strlen($array_suffix);
        if(strpos($key, $array_suffix)!==FALSE){
          $value = isCheckbox($value);
          $newkey = substr($key, $lunghezza_array_suffix);//riscrivo il nome della chiave levando la parte iniziale-> es: array_nome diventa nome
        }else{
          $newkey = $key; //riscrivo il nome della chiave levando via la lingua-> es: nome_it diventa nome
        }        
        $q_up = $db->prepare("UPDATE $database_name SET $newkey = '$value' WHERE id='$id'"); //faccio l'update delle condizioni a database
        $q_up->execute();
      } //fine del controllo che il name dell'input sia in una lingua o totale
    } //fine di: "se la chiave non è ne l'id, ne la pagina in cui siamo, ne il pubblica, faccio la condizione da inserire"
  }
  // $q_up = $db->prepare("UPDATE ". $database_name . " SET " . $condizioni ." WHERE id='".$id."'"); //faccio l'update delle condizioni a database
  // $q_up->execute();


  // $condizioni = ""; //resetto le condizioni e riporto la variabile i a 0
  // $i = 0;
}

// chiudo la connessione a MySQL
	$db = null;
	exit;
?>