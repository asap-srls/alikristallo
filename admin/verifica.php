<?php
$pag='index';
include('include/config.php');
// require_once 'functions/phpPasswordHashingLib-master/passwordLib.php';
$uname=$_POST['u_name'];
$pass=trim($_POST['pass']);

$q = $db->prepare("SELECT * FROM login WHERE user=:user LIMIT 1");
$q->bindValue(":user", $uname);
$q->execute();
 
while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
if(password_verify($pass, $row['password'])){
		// session_start();
		$_SESSION['name']=$row['user'];
		if (isset($_SESSION['url_completa'])) {
			$pos = strpos($_SESSION['url_completa'], '/admin/');
			if (substr($_SESSION['url_completa'], -1)=="/") {
                $newurl = $_SESSION['url_completa'] . "index.php";
            }else{
            	$newurl = $_SESSION['url_completa'];
            }
			header("Location:".substr($newurl, $pos + 7));
		}else{
			header("Location:intro.php");
		}
		
 }else{
 	header("Location:index.php?result=no");
 }
}
?>