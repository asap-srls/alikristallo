<?
$pag=basename(__DIR__);
if (isset($_GET['id']) || $_GET['id']!='') {  
  $id = $_GET['id'];
}else{
  $id="";
}
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';
include '../moduli/moduli.php';

//PREPARAZIONE ALLA PAGINA
// Se è settato l'id allora imposto la modifica in true, il che vuol dire che mi farà
// il collegamento a database per prendere i dati, se invece non è settato prendo l'ultimo id inserito, lo aumento di 1
// e lo setto come id

$hasFile = true; //setto se la pagina ha oppure no dei file, verrà richiamata la funzione giusta nel footer
$dbTable = "prodotti";
$dbTable_it = "prodotti_it";

if (!isset($id) || $id == "") {
  $q_id = $db->prepare("SELECT * FROM $dbTable_it ORDER BY id DESC LIMIT 1");
  $q_id->execute();
  $data_id = $q_id->fetch(PDO::FETCH_ASSOC);
  $id= $data_id['id']+1;
  $modifica = false;
  
  $image = NULL; //setto che non c'è l'immagine 
  $image_bg = NULL; //setto che non c'è l'immagine 
  $data_online = "";  
  
}else{    
  $q0 = $db->prepare("SELECT * FROM $dbTable_it WHERE id=:id LIMIT 1");
  $q0->bindValue(":id", $id);
  $q0->execute();
  $data0 = $q0->fetch(PDO::FETCH_ASSOC);
  $modifica = true;
  
  //cosa mi serve non in lingua?  
  $image = $data0['img']; //setto che ci siano le immagini - multiple img
  $image_bg = $data0['img_bg']; //1 immagine
  $data_online = $data0['pubblica']; //simpleCheck  
}
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <div class="col-md-9 col-lg-10 main">
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>
            
              <h1 class="display-1 hidden-xs-down">
               <? echo $pag ?>
              </h1>
              <form method="post" id="fileForm" enctype="multipart/form-data">
                <?
                  //setto la cartella delle immagini
                  $cartella_img = $pag.'/'.$id;
                  echo '<input type="hidden" name="id" value="'.$id.'">';
                  // IMPORTANTE, il value del name="pag" dev'essere la cartella dove vanno inserite le immagini
                  // quindi mettere 
                  echo '<input type="hidden" name="pag" value="'.$cartella_img.'">';
                  echo '<input type="hidden" name="db_name" value="'.$dbTable.'">';
                  //////////////INPUT FILE/////////////
                  //Quante immagini si possono inserire nel file input e negli alt successivi?
                  echo "<h5 class='col-12 pt-3'>Immagini del prodotto</h5>";
                  $input_img = 4;                                    
                  images ("img", $input_img, "960 x 1440 px - png", $image, $pathsito, $cartella_img, $dbTable, $id);

                  echo "<h5 class='col-12 pt-3'>Immagine di background</h5>";
                  $input_img_bg = 1;                                    
                  images ("img_bg", $input_img_bg, "2000 x 1550 px - png", $image_bg, $pathsito, $cartella_img, $dbTable, $id);
                ?>
              </form>
                      
              <form method="post" id="mainForm">
            <? 
              echo '<input type="hidden" id="id" name="id" value="'.$id.'">';
            ?>
            <input type="hidden" name="pag" id="pag" value="<? echo $pag ?>">
            <input type="hidden" name="db_name" id="db_name" value="<? echo $dbTable ?>">
            <input type="hidden" name="numero" id="numero" value="<? echo $id ?>">
                                   
              <hr>

              <div class="row">
                <?                  
                  simpleCheckbox("pubblica", NULL, "Pubblicato on-line", "si", $data_online);                  
                ?>
              </div>
              <div class="col-lg-12">
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                    <? 
                    $i = 0;
                    foreach ($langs as $lang_key) {
                      $table_db = $dbTable."_".$lang_key;
                      $i++;
                      if ($modifica!=false) {
                          $q = $db->prepare("SELECT * FROM $table_db WHERE id=:id LIMIT 1");
                          $q->bindValue(":id", $id);
                          $q->execute();
                          $data = $q->fetch(PDO::FETCH_ASSOC);

                          $data_nome = $data['nome']; //input
                          $data_title = $data['title']; //input
                          $data_description = $data['description']; //input
                          $data_alt_img = $data['alt_img']; //input
                          $data_suolo = $data['suolo']; //input anche se a db è messo text
                          $data_vitigno = $data['vitigno']; //input anche se a db è messo text
                          $data_densita = $data['densita']; //input anche se a db è messo text
                          $data_eta = $data['eta']; //input anche se a db è messo text
                          $data_impianto = $data['impianto']; //input anche se a db è messo text
                          $data_estensione = $data['estensione']; //input anche se a db è messo text
                          $data_produzione = $data['produzione']; //input anche se a db è messo text
                          $data_vinificazione = $data['vinificazione']; //input
                          
                      }else{
                          $data_nome = "";
                          $data_title = "";
                          $data_description = "";
                          $data_alt_img = "";
                          $data_suolo = "";
                          $data_vitigno = "";
                          $data_densita = "";
                          $data_eta = "";
                          $data_impianto = "";
                          $data_vinificazione = "";
                          
                      }
                      ?>                                    
                      <div class="card-header" role="tab" id="heading<? echo $i ?>"  data-toggle="collapse" data-parent="#accordion" href="#collapse<? echo $i ?>" aria-expanded="true" aria-controls="collapse<? echo $i ?>">
                                <? echo $lang_key; ?>
                      </div>
                      <div id="collapse<? echo $i ?>" class="card-block collapse <? echo ($i==1 ? "show":"") ?>" role="tabpanel" aria-labelledby="heading<? echo $i ?>">
                        <div class="row">
                          <?
                          multi_input("alt_img", $lang_key, "piccola descrizione immagine", $data_alt_img, $input_img);
                          meta_tag($lang_key, $data_title, $data_description);                          
                          input("nome",$lang_key, "Nome del prodotto", $data_nome, "text");
                          input("suolo",$lang_key, "Suolo", $data_suolo, "text");
                          input("densita",$lang_key, "Densità media terreno", $data_densita, "text");
                          input("vitigno",$lang_key, "Vitigno", $data_vitigno, "text");
                          input("eta",$lang_key, "Età media impianto", $data_eta, "text");
                          input("impianto",$lang_key, "Tipo d'impianto", $data_impianto, "text");
                          input("estensione",$lang_key, "Estensione", $data_estensione, "text");
                          input("produzione",$lang_key, "Produzione", $data_produzione, "text");
                          text_area("vinificazione", $lang_key, "Vinificazione", $data_vinificazione);                          
                          ?>
                          
                        </div>
                      </div>
                      <? } ?>                                                      
                    </div>
                  </div>
              </div><!--/col-->
              
              
              <div class="col-lg-12 text-right save-btn">
                <a id="submit" class="btn red-bg">SALVA <i class="fa fa-chevron-right fa-lg"></i></a>
              </div>
            </form> 
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserimento <? echo $pag ?></h4>
            </div>
            <div class="modal-body" id="contenutoModal">
                
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary-outline" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<? 
include '../include/footer.php'; 
// chiudo la connessione a MySQL
  $db = null;
?>
  </body>
</html>