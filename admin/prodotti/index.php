<?
$pag=basename(__DIR__);
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';
$dbTable = "prodotti";
$dbTable_it = "prodotti_it";
$q = $db->prepare("SELECT * FROM $dbTable_it order by numero");
$q->execute();
$titoli = array("Sposta","Nome","On-line","Modifica","Elimina");
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>
    
</head>
  <body >
    <?
    	include '../include/header.php'; 
    ?>
    <div class="container-fluid" id="main">
	    <div class="row row-offcanvas row-offcanvas-left">
	        <? include '../include/left_nav.php'; ?>
	        <div class="col-md-9 col-lg-10 main">
	            <p class="hidden-md-up">
	                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
	            </p>

	            <h1 class="display-1 hidden-xs-down">
	             <? echo $pag ?>
	            </h1>
	            <p class="lead hidden-xs-down">Gestisci la sezione <? echo $pag ?></p>

	            <div class="alert alert-success fade collapse" role="alert" id="myAlert">
	               
	                <strong>Tutto ok</strong> elemento modificato con successo
	            </div>
	            <div class="alert alert-danger fade collapse" role="alert" id="myAlertElimina">
	               
	                <strong>Tutto ok</strong> elemento eliminato con successo
	            </div>
	            <hr>
	            <span class="btn btn-danger btn-salva" id="salvaCambiamenti" style="display:none">SALVA</span>
	            <input type="hidden" id="tipologia_selezionata" value="<? echo $pag ?>">
	            <div class="container-fluid">
	                <div class="row mb-3">
	                    <div class="table-responsive">
	                        <table class="table table-striped">
	                            <thead class="thead-inverse">
	                                <tr>
	                                <?
	                                    
	                                    foreach ($titoli as $c => $v) {
	                                        echo "<th>".$v."</th>";
	                                    }
	                                ?>                                
	                                </tr>
	                            </thead>
	                            <tbody id="sortThis">
	                            <? 
	                                while($dati = $q->fetch(PDO::FETCH_ASSOC)){
	                            ?>
	                                <tr id="<? echo $dati['id'] ?>" style="cursor:move">
	                                    <td class="move"><i class="fa fa-arrows" aria-hidden="true"></i>
	                                    <td><? echo $dati['nome']; ?></td>
	                                    <td>
	                                        <div class="checkbox">
	                                            <label style="font-size: 1em">
	                                                <input type="checkbox" name="pubblica" class="splash_pubb" data-datab="<? echo $dbTable ?>" data-campo="pubblica" value="si" <? echo ($dati['pubblica']=='si'?"checked":"") ?>>
	                                                <span class="cr"><i class="cr-icon fa fa-bomb"></i></span>
	                                                on-line
	                                            </label>
	                                        </div>
	                                    </td>
	                                    <td class="modify"><a href="<? echo $root . $pag ?>/nuovo.php?id=<? echo $dati['id'] ?>"><i class="fa fa-share" aria-hidden="true"></i></a></td>
	                                    <td class="delete"><a class="eliminaDato" data-tabella_db="<? echo $dbTable ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
	                                </tr> 
	                            <? } ?>
	                             
	                            </tbody>
	                        </table>
	                        
	                    </div>
	                </div>
	            </div>     
	            <hr>            
	        </div>
	    </div>

	</div>
    <?
    	include '../include/footer.php';
    ?>
    
  </body>
</html>