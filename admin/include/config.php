<?
include_once __DIR__.'/../../include/settings.php';


function getBaseURL() {
	if ( (isset($_SERVER["HTTPS"])) && (
		$_SERVER["HTTPS"] == "on") ) $base_url = "https://" ;
	else $base_url = 'http://' ;
	if ($_SERVER["SERVER_PORT"] != "80" and $_SERVER["SERVER_PORT"]!="443") {
		$base_url .= $_SERVER["SERVER_NAME"].
		":".$_SERVER["SERVER_PORT"] ;
	} else {
		$base_url .= $_SERVER["SERVER_NAME"] ;
	}
	return $base_url ;
}
function getCurrentPageURL() {
	return getBaseURL().$_SERVER["REQUEST_URI"];
}

switch (ENVIRONMENT)
{
	case 'development':
		$root= getBaseURL().'/'. basename(dirname(dirname(__DIR__))) . '/admin/';
		$pathsito= getBaseURL().'/'. basename(dirname(dirname(__DIR__))).'/';

		if (basename(__DIR__)!="admin") { //siamo nelle pagine interne dell'area amministrativa
			// $root= getBaseURL().'/'. basename(dirname(dirname(__DIR__))) . '/admin/';
			// $pathsito= getBaseURL().'/'. basename(dirname(dirname(__DIR__))).'/';
			$path ='../../img/'; //per inserire le immagini
		}else{
			// $root= getBaseURL().'/'. basename(dirname(__DIR__)) . '/admin/';
			// $pathsito= getBaseURL().'/'. basename(dirname(__DIR__)).'/';
			$path ='../img/'; //per inserire le immagini
		}
		$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/".basename(dirname(dirname(__DIR__))). "/admin/";
		include $INC_DIR.'../include/connections.php';
		
	break;

	case 'staging':
		$root= getBaseURL().'/admin/';
		$pathsito = getBaseURL().'/';
		$path ='../../img/'; //per inserire le immagini
		$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/".basename(dirname(__DIR__))."/";	
		include $INC_DIR.'../../include/connections.php';
		
	break;

	case 'on-line':
		$root= getBaseURL().'/admin/';
		$pathsito = getBaseURL().'/';
		$path ='../../img/'; //per inserire le immagini
		$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/".basename(dirname(__DIR__))."/";	
		include $INC_DIR.'../../include/connections.php';
		
	break;

}


function get_page_url() {
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $sp = strtolower($_SERVER["SERVER_PROTOCOL"]);
    $protocol = substr($sp, 0, strpos($sp, "/")) . $s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
}
$url_completa = get_page_url();

$file_path = $INC_DIR."../dictionaries/it.properties";
$lines = explode("\n", trim(file_get_contents($file_path)));
$convert = array();

foreach ($lines as $line) {
	$line = trim($line);

        if (!$line || substr($line, 0, 1) == '#') // Saltiamo le linee vuote e i commenti
        continue;

        if (false !== ($pos = strpos($line, '='))) {
        	$convert[trim(substr($line, 0, $pos))] = trim(substr($line, $pos + 1));

            // se non è presente la stringa di testo mi fa vedere a video la chiave
        	if (trim(substr($line, $pos + 1))=="" or trim(substr($line, $pos + 1))==null) {
        		$convert[trim(substr($line, 0, $pos))] = trim(substr($line, 0, $pos));
        	}
        }
}
//definisco un array con tutte le sezioni da mettere all'interno del menù
$mod = array("home"=>"intro.php",
	"prodotti"=>"prodotti/",
	"sito"=>array(
		"lingue"=>"lingue/",
		"immagini"=>"immagini/"
		),
	"tutorial"=>"tutorial/");

function urlSanitize($string) {
  $string = preg_replace("`\[.*\]`U","",$string);
  $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
  $string = htmlentities($string, ENT_COMPAT, 'utf-8');
  $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","", $string );
  $string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
  return strtolower(trim($string, '-'));
    }

function is_session_started(){
    	if ( php_sapi_name() !== 'cli' ) {
    		if ( version_compare(phpversion(), '5.4.0', '>=') ) {
    			return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
    		} else {
    			return session_id() === '' ? FALSE : TRUE;
    		}
    	}
    	return FALSE;
}

if ( is_session_started() === FALSE ) session_start();


if (!isset($pag)) {
	$pag = "";
}
if ($pag!="index") {
	if(!isset($_SESSION['name'])){
		$_SESSION['url_completa'] = $url_completa;
		header("Location:".$root."index.php?result=data");		
	}
}else{
	if (isset($_SESSION['name'])) {
		header("Location:intro.php");
	}
}
?>