<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<? echo $root ?>css/styles.css?v=2" media="all"/>
    <script src="https://use.fontawesome.com/a6a3497361.js"></script>
    <!-- <script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script> -->
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdn.rawgit.com/tonystar/bootstrap-float-label/v4.0.2/bootstrap-float-label.min.css"/>

    <!--[if !IE]><!-->
  
  <!--<![endif]-->

  <script type="text/javascript">
  	var path = "<?= $root ?>";
  </script>