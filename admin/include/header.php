    <nav class="navbar navbar-fixed-top navbar-toggleable-sm navbar-inverse bg-danger mb-3">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pb-2" href="<? echo $pathsito ?>"><? echo $brand_name ?></a>
        <div class="navbar-collapse collapse" id="collapsingNavbar">
            <ul class="navbar-nav">
            <?
                foreach ($mod as $nome => $link) {
                    if (is_array($link)) { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $nome ?></a>
                            <div class="dropdown-menu">
                       <? foreach ($link as $k => $v) { ?>
                            
                                  <a class="dropdown-item" href="<?= $root.$v ?>"><?= $k ?></a>                                  
                                
                       <? }
                        echo "</div>
                            </li>";
                    }else{
            ?>
                 
                <li class="nav-item <? echo ($nome==$pag ? 'active':'') ?>">
                    <a class="nav-link" href="<? echo $root.$link ?>"><? echo $nome . ($nome==$pag ? '<span class="sr-only">'.$nome.'</span>':'') ?></a>
                </li>
            <? 
                    }
                }
            ?>                
                <!-- <li class="nav-item">
                    <a class="nav-link" href="#myAlert" data-toggle="collapse">Wow</a>
                </li> -->
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="" data-target="#myModal" data-toggle="modal"><i class="fa fa-power-off fa-2x"></i></a>
                </li>
            </ul>
        </div>
    </nav>