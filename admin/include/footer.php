     <!-- Modal actions -->
<div class="modal fade" id="modalInput" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="contenuto_ajax">
          
        </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Esci dal pannello</h4>
            </div>
            <div class="modal-body">
                Esci dal pannello di controllo
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a class="btn btn-secondary" href="<? echo $root ?>logout.php">OK</a>
            </div>
        </div>
    </div>
</div>
<footer class="col-12">
    <p class="text-right small">©<? echo date("Y") . $brand_name ?></p>
</footer>
    <!--scripts loaded here-->
    
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>    
    <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
    <script src="<? echo $root ?>js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo $root ?>js/dist/jquery.inputmask.bundle.js"></script>
    <script type="text/javascript" src="<?php echo $root ?>js/dist/inputmask/bindings/inputmask.binding.js"></script>   
    <script type="text/javascript">
      $(document).on('click', '#submit', function() {          
            var form = $("#mainForm")[0];
            var str = new FormData(form);
            $('.simplecheck_val').each(function(key, val){
               if($(this).is(":checked")){
                   str.append($(this).attr('name'), $(this).val());                   
                }else{
                   str.append($(this).attr('name'), null);
                }
            });
            <? if (isset($hasFile) AND $hasFile != false) { ?>
                var fileForm = $("#fileForm")[0];
                var strFile = new FormData(fileForm);      
            <? } ?>
            //chiamata ajax
            $.ajax({    
            type: "POST",  
            url: "<? echo $root ?>functions/insert_db.php",
            data: str,              
            contentType: false,
            processData: false,
            success: function()
            {
            <? if (isset($hasFile) AND $hasFile != false) { ?>
              $.ajax({
                type: "POST",
                url: "<? echo $root ?>functions/insert_file.php",
                data: strFile,              
                contentType: false,
                processData: false,
                success: function(msg)
                {
                  $("#contenutoModal").html(msg);
                  $("#successModal").modal('show');
                },
                error: function()
                {
                  alert("errore nella seconda chiamata"); //sempre meglio impostare una callback in caso di fallimento
                }
              });
              <? } else{ ?>
                    $("#contenutoModal").html("Tutto ok! elemento salvato nel database");
                    $("#successModal").modal('show');
             <? } ?>
            },
            error: function()
            {
              alert("errore nella prima chiamata"); //sempre meglio impostare una callback in caso di fallimento
            }
          });
         });
    </script>

