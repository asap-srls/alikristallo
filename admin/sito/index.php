<?
$pag=basename(__DIR__);
$id = 1;
include('../include/config.php');
include '../include/languages.php';
include '../functions/session.php';
include '../moduli/moduli.php';

//PREPARAZIONE ALLA PAGINA
// Se è settato l'id allora imposto la modifica in true, il che vuol dire che mi farà
// il collegamento a database per prendere i dati, se invece non è settato prendo l'ultimo id inserito, lo aumento di 1
// e lo setto come id

$hasFile = true; //setto se la pagina ha oppure no dei file, verrà richiamata la funzione giusta nel footer
$dbTable_it = $pag."_it";

if (!isset($id) || $id == "") {
  $q_id = $db->prepare("SELECT * FROM $dbTable_it ORDER BY id DESC LIMIT 1");
  $q_id->execute();
  $data_id = $q_id->fetch(PDO::FETCH_ASSOC);
  $id= $data_id['id']+1;
  $modifica = false;
}else{    
  $q0 = $db->prepare("SELECT * FROM $dbTable_it WHERE id=:id LIMIT 1");
  $q0->bindValue(":id", $id);
  $q0->execute();
  $data0 = $q0->fetch(PDO::FETCH_ASSOC);
  $modifica = true;
}
?>
<!DOCTYPE html>
<html>
  <html lang="en">
  <head>
  <? include '../include/head.php' ?>    
</head>
  <body >
    <? include '../include/header.php'; ?>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <? include '../include/left_nav.php'; ?>
        <div class="col-md-9 col-lg-10 main">
            <p class="hidden-md-up">
                <button type="button" class="btn btn-primary-outline btn-sm" data-toggle="offcanvas"><i class="fa fa-chevron-left"></i> Menu</button>
            </p>
            
              <h1 class="display-1 hidden-xs-down">
               <? echo $pag ?>
              </h1>
              <form method="post" id="fileForm" enctype="multipart/form-data">
                <?
                  echo '<input type="hidden" name="id" value="'.$id.'">';
                  echo '<input type="hidden" name="pag" value="'.$pag.'">';
                  //////////////INPUT FILE/////////////
                  //Quante immagini si possono inserire nel file input e negli alt successivi?
                  $input_img = 1;                                    
                  images ("home_foto1", $input_img, "HOME 1 - 900 x 900 px", $data0['home_foto1'], $pathsito, $pag, $id);

                  $input_img2 = 1;                                    
                  images ("home_foto2", $input_img2, "HOME 2 - 900 x 900 px", $data0['home_foto2'], $pathsito, $pag, $id);

                  $input_img3 = 1;                                    
                  images ("home_foto3", $input_img3, "HOME 3 - 900 x 900 px", $data0['home_foto3'], $pathsito, $pag, $id);

                  $input_img4 = 1;                                    
                  images ("home_foto4", $input_img4, "HOME 4 - 900 x 900 px", $data0['home_foto4'], $pathsito, $pag, $id);

                  $input_img4 = 8;                                    
                  images ("about_foto", $input_img4, "ABOUT - 1280 x 853 px", $data0['about_foto'], $pathsito, $pag, $id);
                ?>
              </form>          
              <form method="post" id="mainForm">
            <? 
              echo '<input type="hidden" id="id" name="id" value="'.$id.'">';
            ?>
            <input type="hidden" name="pag" id="pag" value="<? echo $pag ?>">
                                   
              <hr>
              
              <div class="col-lg-12">
                  <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                    <?
                    $i = 0;
                    foreach ($langs as $lang_key) {
                      $dbTable = $pag."_".$lang_key;
                      $i++;
                      if ($modifica!=false) {
                          $q = $db->prepare("SELECT * FROM $dbTable WHERE id=:id LIMIT 1");
                          $q->bindValue(":id", $id);
                          $q->execute();
                          $data = $q->fetch(PDO::FETCH_ASSOC);
                      }
                      ?>                                    
                      <div class="card-header" role="tab" id="heading<? echo $i ?>"  data-toggle="collapse" data-parent="#accordion" href="#collapse<? echo $i ?>" aria-expanded="true" aria-controls="collapse<? echo $i ?>">
                                <? echo $lang_key; ?>
                      </div>
                      <div id="collapse<? echo $i ?>" class="card-block collapse <? echo ($i==1 ? "show":"") ?>" role="tabpanel" aria-labelledby="heading<? echo $i ?>">
                        <div class="row">
                          <?
                          echo "<h3 class='col-12'>HOME PAGE</h3>";
                          /////////////INPUT SEMPLICI///////////////                          
                          input("home_title", $lang_key, "title - SEO", $data['home_title']);                          
                          input("home_description", $lang_key, "description - SEO", $data['home_description']);
                          input("home_titolo1", $lang_key, "titolo della prima foto", $data['home_titolo1']);
                          input("home_testo1", $lang_key, "testo della prima foto", $data['home_testo1']);
                          input("home_titolo2", $lang_key, "titolo della seconda foto", $data['home_titolo2']);
                          input("home_testo2", $lang_key, "testo della seconda foto", $data['home_testo2']);
                          input("home_titolo3", $lang_key, "titolo della terza foto", $data['home_titolo3']);
                          input("home_testo3", $lang_key, "testo della terza foto", $data['home_testo3']);
                          input("home_titolo4", $lang_key, "titolo della quarta foto", $data['home_titolo4']);
                          input("home_testo4", $lang_key, "testo della quarta foto", $data['home_testo4']);
                          multi_input("link_img", $lang_key, "link all'immagine n. ", $data['link_img'], 4);
                          
                          

                          echo "<h3 class='col-12'>ABOUT</h3>";
                          input("about_title", $lang_key, "title - SEO", $data['about_title']);                          
                          input("about_description", $lang_key, "description - SEO", $data['about_description']);
                          /////////////////TEXT AREA///////////////
                          text_area("about_descrizione", $lang_key, "Inserisci la descrizione della pagina about", $data['about_descrizione']);

                          echo "<h3 class='col-12'>CONTATTI</h3>";
                          input("contact_title", $lang_key, "title - SEO", $data['contact_title']);                          
                          input("contact_description", $lang_key, "description - SEO", $data['contact_description']);
                          /////////////////TEXT AREA///////////////
                          text_area("contact_descrizione", $lang_key, "Inserisci la descrizione della pagina contatti", $data['contact_descrizione']);
                          ?>
                          
                        </div>
                      </div>
                      <? } ?>                                                      
                    </div>
                  </div>
              </div><!--/col-->
              <div class="col-lg-12 text-right save-btn">
                <a id="submit" class="btn red-bg">SALVA <i class="fa fa-chevron-right fa-lg"></i></a>
              </div>
            </form> 
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserimento <? echo $pag ?></h4>
            </div>
            <div class="modal-body" id="contenutoModal">
                
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary-outline" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<? 
include '../include/footer.php'; 
// chiudo la connessione a MySQL
  $db = null;
?>
  </body>
</html>