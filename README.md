Funzionamento della vecchia area amministrativa:
. tutti i name degli input sono i nomi dei campi della tabella a database (es: se a database c'è un campo customer_id, l'input avrà il name customer_id)
. dentro la cartella moduli c'è un file moduli.php, con i commenti alle varie funzioni richiamate e ai dati che servono all'interno di ogni funzione.
	Questo file richiama i vari input dell'area amministrativa
. all'interno del file include/languages.php c'è un array con le lingue del sito
. le tabelle a database finiscono tutte col codice della lingua. Es: products_it, products_en, products_es

Siccome il codice è abbastanza vecchio (è del 2016) se vuoi ci sentiamo così ti spiego tutto meglio